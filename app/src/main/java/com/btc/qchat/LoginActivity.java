package com.btc.qchat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class LoginActivity extends AppCompatActivity {

    private Button loginButton;
    private Button registerButton;
    private EditText emailEdit;
    private EditText passwordEdit;
    private CheckBox rememberBox;
    private View parentView;
    private SharedPreferences preferences;

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //setting views
        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerButton);
        emailEdit = findViewById(R.id.emailEdit);
        passwordEdit = findViewById(R.id.passwordEdit);
        rememberBox = findViewById(R.id.rememberBox);
        parentView = findViewById(R.id.loginLayout);

        auth = FirebaseAuth.getInstance();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewUser();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });
    }

    private void createNewUser(){
        String email = emailEdit.getText().toString();
        String password = passwordEdit.getText().toString();

        if (!checkEmail(email)) return;
        if (!checkPassword(password)) return;

        Snackbar.make(parentView, R.string.login_wait, Snackbar.LENGTH_LONG).show();

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                         if (task.isSuccessful()) {
                             changeDisplayName();
                             startActivity(new Intent(LoginActivity.this, MainActivity.class));
                         }
                         else {
                             Snackbar.make(parentView, R.string.unknown_error, Snackbar.LENGTH_LONG).show();
                         }
                    }
                });
    }
    private void changeDisplayName(){ //TODO
        FirebaseUser user = auth.getCurrentUser();
        assert user != null;

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName("John Doe")
                .build();
        user.updateProfile(profileUpdates);
    }

    private void loginUser() {
        String email = emailEdit.getText().toString();
        String password = passwordEdit.getText().toString();
        if (!checkEmail(email)) return;
        if (!checkPassword(password)) return;

        Snackbar.make(parentView, R.string.login_wait, Snackbar.LENGTH_LONG).show();

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) startActivity(new Intent(LoginActivity.this, MainActivity.class));
                else Snackbar.make(parentView, R.string.unknown_error, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private boolean checkEmail(String email) {
        Snackbar message = Snackbar.make(parentView, R.string.invalid_email, Snackbar.LENGTH_LONG);
        if (!email.contains("@")){
            message.show();
            return false;
        }
        return true;
    }

    private boolean checkPassword(String password) {
        if (password.isEmpty()){
            Snackbar.make(parentView, R.string.empty_password, Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (password.length() < 8){
            Snackbar.make(parentView, R.string.short_password, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}