package com.btc.qchat;

public class SingleMessage {
    private String author;
    private String message;

    public SingleMessage() {
    }

    public SingleMessage(String author, String message) {
        this.author = author;
        this.message = message;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }
}
