package com.btc.qchat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChatFragment extends Fragment {

    private String mDisplayName;
    private String mMessageEditText;
    private Button mSendButton;
    private AppCompatEditText mEditText;
    private DatabaseReference mDatabaseReference;

    public ChatFragment(String displayName) {
        mDisplayName = displayName;
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        View v = inflater.inflate(R.layout.fragment_chat, container, false);

        //getting member variables set
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mSendButton = v.findViewById(R.id.sendButton);
        mEditText = v.findViewById(R.id.messageET);

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        return v;
    }

    public void sendMessage() {
        String userInput = mEditText.getText().toString();
        SingleMessage message = new SingleMessage(mDisplayName, userInput);
        mDatabaseReference.child("messages").push().setValue(message);
        mEditText.setText("");
    }
}