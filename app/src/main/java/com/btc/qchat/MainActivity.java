package com.btc.qchat;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends AppCompatActivity {

    FirebaseUser mUser;
    FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //creating fragment manager
        mFragmentManager = getSupportFragmentManager();

        //add loading
        mFragmentManager.beginTransaction()
                .add(R.id.fragment_layout, new LoadingFragment())
                .commit();

        //check if user is logged
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mUser == null) {
            Intent launchLogin = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(launchLogin);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mUser != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_layout, new ChatFragment("John Doe")) //TODO replace with friendlist
                    .commit();
        }
    }
}